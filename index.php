<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Les formulaires en php</title>
</head>
<body>
      
<h1>V Les formulaires</h1>
<!-- 
Prenez soin d'agrémenter votre code avec de l'HTML 5 valide (DOCTYPE, ....) -->

<h1>**Exercice 1 **</h1>
<!-- Créer un formulaire demandant le nom et le prénom. Ce formulaire doit rediriger
 vers la page user.php avec la méthode GET. -->

 <form action="user.php" method="GET">
 <p>
   <label for="name"> Votre nom :</label></br>
   <input type="text" name="nom" placeholder=" ">
 </p>  
 
 <p>
   <label for="name"> Votre prenom :</label></br>
   <input type="text" name="prenom" placeholder=" ">
 </p>  
 <p>
    <input type="submit" value="Envoyer" >
 </p>   
</form>

<?php


?>
</br>
<h1>Exercice 2</h1>
 <!-- Créer un formulaire demandant le nom et le prénom. Ce formulaire doit rediriger 
 vers la page user.php avec la méthode POST. -->


 <form action="user.php" method="POST">
 <p>
   <label for="name"> Votre nom :</label></br>
   <input type="text" name="nom" placeholder=" ">
 </p>  
 
 <p>
   <label for="name"> Votre prenom :</label></br>
   <input type="text" name="prenom" placeholder=" ">
 </p>  
 <p>
    <input type="submit" value="Envoyer" >
 </p>   
</form>


 <h1>Exercice 3 </h1>
<!-- Avec le formulaire de l'exercice 1, afficher dans la page user.php les données du 
formulaire transmis. -->

<h1>Exercice 4</h1>
 <!-- Avec le formulaire de l'exercice 2, afficher dans la page user.php les données du
  formulaire transmises. -->

<h1>Exercice 5 </h1>
<!-- Créer un formulaire sur la page index.php avec :

    Une liste déroulante pour la civilité (Mr ou Mme)
    Un champ texte pour le nom
    Un champ texte pour le prénom

Ce formulaire doit rediriger vers la page index.php. Vous avez le choix de la méthode. -->



<FORM action="index.php"  method="POST" >

    <p>
    <label for="name"> Votre nom :</label></br>
    <input type="text" name="nom" placeholder=" ">
    </p>  
    
    <p>
    <label for="name"> Votre prenom :</label></br>
    <input type="text" name="prenom" placeholder=" ">
    </p>  

  <SELECT name="sexe" size="1">
    <OPTION>Mr
    <OPTION>Mme
   </SELECT>

    <p>
        <input type="submit" id="envoi"value="Envoyer" >
    </p>  

</FORM>
<p>Dans le formulaire précédent, vous avez fourni les
        informations suivantes :</p>
        
        <?php

        if(isset($_POST["envoi"])){ // si formulaire soumis
         
            echo 'Votre nom : ' .$_POST["nom"].'<br>';
            echo 'Votre prénom : '.$_POST["prenom"].'<br>';
            echo 'Votre civilité : ' .$_POST["sexe"].'<br>';

         }
         else {
           echo "sa marche pas";
       }
        ?>

<h1>Exercice 6 </h1>
<!-- Avec le formulaire de l'exercice 5, Si des données sont passées en POST ou en GET,
 le formulaire ne doit pas être affiché. Par contre les données transmises doivent l'être. 
 Dans le cas contraire, c'est l'inverse. N'utiliser qu'une seule page. -->

 <h1>Exercice 7 </h1>
<!-- Au formulaire de l'exercice 5, ajouter un champ d'envoi de fichier. Afficher en plus de ce 
qui est demandé à l'exercice 6, le nom et l'extension du fichier. -->

<h1>Exercice 8 </h1>
<!-- Sur le formulaire de l'exercice 6, en plus de ce qui est demandé sur les exercices précédent, 
vérifier que le fichier transmis est bien un fichier pdf. -->


</body>
</html>